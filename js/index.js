$('#exampleModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever')
    //var email = 'nombre@email.com' // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text('Nuevo Mensaje para ' + recipient)
    //modal.find('.modal-body input').val(recipient)  
    //modal.find('.modal-body input').val(email)     
})

$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    //JQuery modal en consola
    $('#contactoBtn').removeClass('btn-group-lg');
    $('#contactoBtn').addClass('btn-primary');
    //$('#contactoBtn').prop('disabled', true);

    $('#exampleModal').on('show.bs.modal', function (e) {
        console.log('el modal se esta mostrando');
    })
    $('#exampleModal').on('shown.bs.modal', function (e) {
        console.log('el modal se mostrando');
    })
    $('.carousel').carousel({
        interval: 2500
    })
})

$(function () {
    $('.example-popover').popover({
        container: 'body'
    });
})

